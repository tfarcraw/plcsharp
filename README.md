# PLCSharp

#### 介绍
PLC风格的上位机程序。

![image](Demo.fw.png)

#### 初始化数据库
```
dotnet tool install --global dotnet-ef

cd .\ModuleCore\src\

dotnet ef migrations add InitialCreate

dotnet ef database update
```