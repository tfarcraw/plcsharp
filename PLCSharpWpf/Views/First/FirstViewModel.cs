﻿using CommunityToolkit.Mvvm.ComponentModel;
using Prism.Ioc;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Printing;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Media;
using System.Windows.Media.Imaging;

namespace PLCSharp.Views.First
{
    public class FirstViewModel : ObservableObject
    {
        public FirstViewModel()
        {
        }

        public FirstViewModel(IContainerExtension container)
        {
            PLC = container.Resolve<PLC.PLC>();
            HMI = container.Resolve<HMI.HMI>();

        }

        public PLC.PLC PLC { get; set; }
        public HMI.HMI HMI { get; set; }
        private WriteableBitmap _imgSource = new(1200, 600, 96.0, 96.0, PixelFormats.Bgr24, null);
        public WriteableBitmap ImgSource
        {
            get
            {
                return _imgSource;
            }
            set
            {
                SetProperty(ref _imgSource, value);
            }
        }
        private readonly ConcurrentQueue<PointF> PointsQueue = new();
        readonly List<PointF> Points = [];
        public async void ExecuteDraw()
        {
            while (true)
            {
                await Task.Delay(100);
                if (PointsQueue.IsEmpty) continue;

                while (!PointsQueue.IsEmpty)
                {
                    PointsQueue.TryDequeue(out PointF point);
                    Points.Add(point);
                }
                Application.Current?.Dispatcher.Invoke(delegate
                {
                    WriteableBitmap writeableBitmap = _imgSource;
                    int width = (int)writeableBitmap.Width;
                    int height = (int)writeableBitmap.Height;
                    writeableBitmap.Lock();
                    Graphics graphics = Graphics.FromImage(new Bitmap(width, height, writeableBitmap.BackBufferStride, System.Drawing.Imaging.PixelFormat.Format24bppRgb, writeableBitmap.BackBuffer));
                    graphics.Clear(System.Drawing.Color.Black);
                    //System.Drawing.FontFamily family = new("微软雅黑");

                    //求最小点，看是否为负；为负则整体偏移
                    var min_x = Points.Min(p => p.X);
                    var min_y = Points.Min(p => p.Y);

                    var points = Points.ToArray();
                    if (min_x < 0)
                        for (int i = 0; i < Points.Count; i++)
                        {
                            points[i] = new PointF(Points[i].X - min_x, Points[i].Y);
                        }
                    if (min_y < 0)
                        for (int i = 0; i < Points.Count; i++)
                        {
                            points[i] = new(Points[i].X, Points[i].Y - min_y);
                        }
                    for (int i = 0; i < Points.Count; i++)
                    {
                        points[i] = new(points[i].X + 100, points[i].Y + 100);
                    }
                    var max_x = points.Max(p => p.X);
                    var max_y = points.Max(p => p.Y);
                    var scalex = max_x / width;
                    var scaley = max_y / height;
                    var scale = Math.Max(scaley, scalex);
                    foreach (var point in points)
                    {

                        graphics.DrawRectangle(new System.Drawing.Pen(System.Drawing.Color.White), new Rectangle((int)(point.X / scale), (int)(point.Y / scale), 1, 1));

                    }

                    graphics.Flush();
                    writeableBitmap.AddDirtyRect(new Int32Rect(0, 0, width, height));
                    writeableBitmap.Unlock();

                });
            }
        }
    }
}