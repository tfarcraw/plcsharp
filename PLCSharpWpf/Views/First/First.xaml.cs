﻿using ModuleCore.Common;
using ModuleCore.Views.Authority;
using System.Windows.Controls;

namespace PLCSharp.Views.First
{
    [NavigationPage(ViewName = "First",
       IconKind = "\ue622",
       DisplayName = "主界面", UserLevel = Authority.Unauthorized , Index = 1000)]
    public partial class First : UserControl
    {
        public First()
        {
            InitializeComponent();
        }
    }
}