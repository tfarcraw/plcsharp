﻿using CommunityToolkit.Mvvm.ComponentModel;
using Microsoft.EntityFrameworkCore;
using ModuleCore.Common;
using ModuleCore.Mvvm;
using PLCSharp.Models;
using Prism.Commands;
using Prism.Events;
using Prism.Ioc;
using Prism.Mvvm;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace PLCSharp.Views.GlobalVariables
{
    [Model]
    public partial class VariablesModel : BindableBase
    {

        public VariablesModel(IContainerExtension container, IEventAggregator ea)
        {
            _DatasContext = container.Resolve<DatasContext>();
            _EventAggregator = ea;




            foreach (var item in _DatasContext.Variables)
            {
                if (!item.RetainPersistent)
                    item.Value = item.DefaultValue;
                Variables.Add(item);
            }

            AppDomain.CurrentDomain.ProcessExit += OnExit;
            bkgWorker = new BackgroundWorker { WorkerSupportsCancellation = true };
            bkgWorker.DoWork += Background;

            if (!bkgWorker.IsBusy)
                bkgWorker.RunWorkerAsync();
        }
        public BindingList<Variable> Variables { get; set; } = [];

        readonly IEventAggregator _EventAggregator;
        private Variable _SelectedVariable;
        public Variable SelectedVariable
        {
            get { return _SelectedVariable; }
            set { SetProperty(ref _SelectedVariable, value); }
        }
        public DatasContext _DatasContext { get; set; }

        private DelegateCommand _Add;
        public DelegateCommand Add =>
            _Add ??= new DelegateCommand(ExecuteAdd);

        void ExecuteAdd()
        {
            Variables.Add(new());
        }



        private DelegateCommand _Delete;
        public DelegateCommand Delete =>
            _Delete ??= new DelegateCommand(ExecuteDelete);

        void ExecuteDelete()
        {
            if (SelectedVariable != null)
            {
                _DatasContext.Variables.Remove(SelectedVariable);

                _DatasContext.SaveChanges();
                var name = SelectedVariable.Name;
                Variables.Remove(SelectedVariable);
                ShowError($"已删除变量：{name}");
            }

        }

        private DelegateCommand _Save;
        public DelegateCommand Save =>
            _Save ??= new DelegateCommand(ExecuteSave);

        void ExecuteSave()
        {
            var names = new List<string>();

            foreach (var item in Variables)
            {
                if (string.IsNullOrEmpty(item.Name))
                {
                    ShowError("保存失败，无变量名！");
                    return;
                }

                if (names.Contains(item.Name))
                {

                    ShowError("保存失败，重名的变量！");
                    return;

                }
                else
                {
                    names.Add(item.Name);
                }

            }


            foreach (var item in Variables)
            {
                if (!_DatasContext.Variables.Contains(item))
                {

                    _DatasContext.Variables.Add(item);
                }

            }
            _DatasContext.SaveChanges();
            ShowError("保存成功！");
        }
        private readonly BackgroundWorker bkgWorker;
        private void OnExit(object sender, EventArgs e)
        {

            bkgWorker.CancelAsync();
            bkgWorker.Dispose();
        }
        private void Background(object sender, DoWorkEventArgs e)
        {
            var worker = (BackgroundWorker)sender;



            while (!worker.CancellationPending)
            {
                try
                {

                    _DatasContext.SaveChanges();


                }
                catch (Exception)
                {
                    goto sleep;
                }

            sleep:

                Thread.Sleep(1000);
            }
        }
        public void ShowError(string msg)
        {
            _EventAggregator.GetEvent<MessageEvent>().Publish(new()
            {
                Target = "errLog",
                Content = msg
            });
        }
    }
}