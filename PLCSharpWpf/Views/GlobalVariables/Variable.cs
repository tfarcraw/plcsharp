﻿
using CommunityToolkit.Mvvm.ComponentModel;
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using DataType = PLCSharp.Views.GlobalVariables.VariableDataType;

namespace PLCSharp.Views.GlobalVariables
{
    public class Variable
    {
        [Key]
        /// <summary>
        /// 变量名
        /// </summary>
        public string Name { get; set; }
        /// <summary>
        /// 当前值
        /// </summary>
        public string Value
        {
            get { return _value; }
            set
            {
                switch (Type)
                {
                    case DataType.Int32:
                        if (int.TryParse(value, out _))
                        {
                            _value = value;
                        };
                        break;
                    case DataType.DOUBLE:
                        if (double.TryParse(value, out _))
                            _value = value;
                        break;
                    case DataType.STRING:
                        _value = value;

                        break;
                    default:
                        break;
                }
            }
        }



        private string _value;

        /// <summary>
        /// 初始值
        /// </summary>
        public string DefaultValue
        {
            get { return _defaultValue; }
            set
            {
                switch (Type)
                {
                    case DataType.Int32:
                        if (int.TryParse(value,out _))
                        { 
                        _defaultValue = value;
                        };
                        break;
                    case DataType.DOUBLE:
                        if (double.TryParse(value,out _))
                            _defaultValue = value;
                        break;
                    case DataType.STRING:
                        _defaultValue = value;

                        break;
                    default:
                        break;
                }
            }
        }


        private string _defaultValue;
        /// <summary>
        /// 数据类型
        /// </summary>
        public VariableDataType Type { get; set; }



        [Column(TypeName = "INTEGER")]
        /// <summary>
        /// 保持
        /// </summary>
        public bool RetainPersistent { get; set; }
        /// <summary>
        /// 变量注释
        /// </summary>
        public string Comments { get; set; }

    }


}