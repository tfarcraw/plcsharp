﻿using ModuleCore.Common;
using System.Windows.Controls;
using PLCSharp.Models;
using System.Collections.Generic;
using ModuleCore.Views.Authority;
namespace PLCSharp.Views.GlobalVariables
{
    [NavigationPage(ViewName = "GlobalVariables",
       IconKind = "\ue604",
       DisplayName = "全局变量", UserLevel = Authority.Administrator)]
    public partial class GlobalVariables : UserControl
    {
        public GlobalVariables()
        {
            InitializeComponent();
        }
    }
}