﻿using CommunityToolkit.Mvvm.ComponentModel;
using Prism.Ioc;
using System.ComponentModel;

namespace PLCSharp.Views.GlobalVariables
{
    public class GlobalVariablesViewModel : ObservableObject
    {

        public GlobalVariablesViewModel(IContainerExtension container)
        {
            Model = container.Resolve<VariablesModel>();
        }
        public VariablesModel Model { get; set; }

  
    }
}