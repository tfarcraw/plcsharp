﻿using CommunityToolkit.Mvvm.ComponentModel;
using PLCSharp.Views.Projects;
using Prism.Ioc;

namespace PLCSharp.Views.Projects
{
    public class ProjectsViewModel : ObservableObject
    {

        public ProjectsViewModel(IContainerExtension container)
        {

            Model = container.Resolve<ProjectsModel>();
        }

        public ProjectsModel Model { get; set; }

    }
}