﻿using CommunityToolkit.Mvvm.ComponentModel;
using PLCSharp.Views.Projects;
using Prism.Ioc;

namespace PLCSharp.Views.Projects.Script
{
    public class ScriptViewModel : ObservableObject
    {

        public ScriptViewModel(IContainerExtension container)
        {

            Model = container.Resolve<ScriptModel>();
        }

        public ScriptModel Model { get; set; }

    }
}