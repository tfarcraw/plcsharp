﻿using Microsoft.EntityFrameworkCore.Metadata.Internal;
using ModuleCore.Common;
using ModuleCore.Views.Authority;
using System.Windows.Controls;

namespace PLCSharp.Views.Projects.Script
{
    [NavigationPage(ViewName = "Projects",
       IconKind = "\ue664",
       DisplayName = "项目管理", UserLevel = Authority.Administrator, Index = 3)]
    public partial class Script : UserControl
    {
        public Script()
        {
            InitializeComponent();
        }
       
    }
}