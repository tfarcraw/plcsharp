﻿using Natasha.CSharp;
using OpenCvSharp;
using PLCSharp.Models;
using Prism.Commands;
using Prism.Events;
using Prism.Ioc;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace PLCSharp.Views.Projects.Script
{
    public class ScriptModel : ModelBase
    {
        public ScriptModel(IContainerExtension container, IEventAggregator ea) : base(container, ea)
        {
        }

        #region Natasha



        private AssemblyCSharpBuilder sharpBuilder;

        private bool IsCompiled;    //编译
        private bool IsPreheating; //预热
        private string _ScriptCode;

        public string ScriptCode
        {
            get { return _ScriptCode; }
            set
            {
                SetProperty(ref _ScriptCode, value);
                IsCompiled = false;
                if (!string.IsNullOrEmpty(value))
                    File.WriteAllText(@"./Scripts/run.script", value);
            }
        }

        private Assembly assembly;
        private DelegateCommand _Run;
        public DelegateCommand Run =>
             _Run ??= new DelegateCommand(ExecuteRunAsync);

        private async void ExecuteRunAsync()
        {
            if (IsPreheating) goto compile;



        compile:
            if (IsCompiled) goto run;
            try
            {
                ShowError("编译中，请稍等。");
                sharpBuilder = new AssemblyCSharpBuilder();
                //给编译器指定一个随机域
                sharpBuilder.Compiler.Domain = DomainManagement.Random;

                //使用文件编译模式，动态的程序集将编译进入DLL文件中
                // sharpBuilder.UseFileCompile();

                // 也可以使用内存流模式。
                sharpBuilder.UseStreamCompile();
                //如果代码编译错误，那么抛出并且记录日志。
                sharpBuilder.ThrowAndLogCompilerError();
                //如果语法检测时出错，那么抛出并记录日志，该步骤在编译之前。
                sharpBuilder.ThrowAndLogSyntaxError();
                //
                sharpBuilder.Add(ScriptCode);

                //编译得到程序集
                await Task.Run(() => assembly = sharpBuilder.GetAssembly());
                IsCompiled = true;
            }
            catch (Exception e)
            {
                ShowError("编译失败");
                ShowError(e.Message);
                return;
            }
        run:
            try
            {
                //在指定域创建一个 返回图像集 的Func 委托， 把刚才的程序集扔进去，
                var func = NDelegate.UseDomain(sharpBuilder.Compiler.Domain)

                    .Func<Mat, Dictionary<string, Mat>>(@"
                    return NatashaScript.Run(arg);
                    ");

                var mat = new Mat();
                //if (Pool.SelectImage.HasValue)
                //    mat = Pool.SelectImage.Value.Value;

                //调用委托 返回图像集
                if (mat.Empty())
                {
                    mat = new Mat(new Size(400, 400), MatType.CV_8UC3, Scalar.Black);
                    Cv2.Circle(mat, new Point(200, 200), 100, Scalar.Red, 5);
                }

                var listmat = func.Invoke(mat);

                //把图像集加入池
                foreach (var item in listmat)
                {
                    //Pool.Images[item.Key] = item.Value;
                }
                ShowError("运行结束");
            }
            catch (Exception e)
            {
                ShowError("运行失败");
                ShowError(e.Message);
                return;
            }
        }

        Func<Mat, Dictionary<string, Mat>> func;
        #endregion Natasha
    }
}
