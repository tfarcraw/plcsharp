﻿using ICSharpCode.AvalonEdit.CodeCompletion;
using ICSharpCode.AvalonEdit.Editing;
using Microsoft.CodeAnalysis;
//using Microsoft.CodeAnalysis.Completion;
using Microsoft.CodeAnalysis.Diagnostics;
using Microsoft.CodeAnalysis.Formatting;
using Microsoft.CodeAnalysis.Text;
using OpenCvSharp;
using RoslynPad.Editor;
using RoslynPad.Roslyn;
using System;
using System.Collections.Generic;
using System.Collections.Immutable;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Windows;
using System.Windows.Input;
using Document = Microsoft.CodeAnalysis.Document;


namespace PLCSharp.Views.Projects.Script
{
    public class CustomTextEditor : RoslynCodeEditor
    {
        private static Document _document;


        //private void TextArea_TextEntering(object sender, TextCompositionEventArgs e)
        //{
        //    if (e.Text.Length > 0 )
        //    {
        //    }
        //}
        bool TextChaged;
        private void TextArea_TextEntered(object sender, TextCompositionEventArgs e)
        {
            if (e.Text.Length > 0)
            {
                TextChaged = true;

            }
        }

        private readonly RoslynHost _host;

        public CustomTextEditor()
        {
            //TextArea.TextEntering += TextArea_TextEntering;
            TextArea.TextEntered += TextArea_TextEntered;
            //  TextArea.KeyDown += TextArea_KeyDown;
            TextArea.LostKeyboardFocus += TextArea_LostKeyboardFocus;
            ICSharpCode.AvalonEdit.Search.SearchPanel.Install(this);
            // Focus();
            var workingDirectory = Directory.GetCurrentDirectory();

            _host = new CustomRoslynHost(additionalAssemblies: new[]
            {
                    Assembly.Load("RoslynPad.Roslyn.Windows"),
                    Assembly.Load("RoslynPad.Editor.Windows"),

                }, RoslynHostReferences.NamespaceDefault.With(assemblyReferences: new[]
            {
            typeof(object).Assembly,
            typeof(Enumerable).Assembly,
               typeof(Mat).Assembly,

        }));

            var documentId = InitializeAsync(_host, new ClassificationHighlightColors(),
                workingDirectory, string.Empty, SourceCodeKind.Script).Result;

            _document = _host.GetDocument(documentId);
        }

        //private void TextArea_KeyDown(object sender, KeyEventArgs e)
        //{
        //    if (e.Key == Key.Enter)
        //    {
        //    }
        //}

        private void TextArea_LostKeyboardFocus(object sender, KeyboardFocusChangedEventArgs e)
        {
            if (TextChaged || Text.Length != Document.Text.Length)
            {
                FormatCodeAsync();
                TextChaged = false;
            }
        }

        private async void FormatCodeAsync()
        {
            var textDocument = _document.WithText(SourceText.From(Document.Text));

            var formatDoc = await Formatter.FormatAsync(textDocument);

            var text = await formatDoc.GetTextAsync();

            Document.Text = text.ToString();
            Text = Document.Text;

        }

        public event PropertyChangedEventHandler PropertyChanged;

        /// <summary>
        /// Raises a property changed event
        /// </summary>
        /// <param name="property">The name of the property that updates</param>
        public void RaisePropertyChanged(string property)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(property));
        }

        // <summary>
        /// A bindable Text property
        /// </summary>
        public new string Text
        {
            get
            {
                return (string)GetValue(TextProperty);
            }
            set
            {
                SetValue(TextProperty, value);

            }
        }

        public static readonly DependencyProperty TextProperty =
            DependencyProperty.Register(
                nameof(Text),
                typeof(string),
                typeof(CustomTextEditor),
                new FrameworkPropertyMetadata
                {
                    DefaultValue = default(string),
                    BindsTwoWayByDefault = true,
                    PropertyChangedCallback = OnDependencyPropertyChanged
                }
            );

        /// <summary>
        /// 属性改变回调
        /// </summary>
        /// <param name="obj"></param>
        /// <param name="args"></param>
        protected static void OnDependencyPropertyChanged(DependencyObject obj, DependencyPropertyChangedEventArgs args)
        {
            var target = (CustomTextEditor)obj;

            if (target.Document != null)
            {
                //var caretOffset = target.CaretOffset;
                var newValue = args.NewValue;
                newValue ??= "";
                var text = (string)newValue;
                target.Document.Text = text;
            }
        }
    }

    public class CustomRoslynHost : RoslynHost
    {
        private bool _addedAnalyzers;

        public CustomRoslynHost(IEnumerable<Assembly>? additionalAssemblies = null, RoslynHostReferences? references = null, ImmutableArray<string>? disabledDiagnostics = null) : base(additionalAssemblies, references, disabledDiagnostics)
        {
        }

        protected override IEnumerable<AnalyzerReference> GetSolutionAnalyzerReferences()
        {
            if (!_addedAnalyzers)
            {
                _addedAnalyzers = true;
                return base.GetSolutionAnalyzerReferences();
            }

            return Enumerable.Empty<AnalyzerReference>();
        }
    }
}
