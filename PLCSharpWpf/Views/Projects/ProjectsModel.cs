﻿using PLCSharp.Models;
using PLCSharp.Views.GlobalVariables;
using PLCSharp.Views.Projects.TaskManager;
using Prism.Commands;
using Prism.Events;
using Prism.Ioc;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PLCSharp.Views.Projects
{
    public class ProjectsModel: ModelBase
    {
        public ProjectsModel(IContainerExtension container, IEventAggregator ea) : base(container, ea)
        {

 
        }
        protected override void OnExit(object sender, EventArgs e)
        {

        }

        private BindingList<FlowTask> _Tasks = [];
        public BindingList<FlowTask> Tasks
        {
            get { return _Tasks; }
            set { SetProperty(ref _Tasks, value); }
        }

        private DelegateCommand _SaveTask;
        public DelegateCommand SaveTask =>
            _SaveTask ??= new DelegateCommand(ExecuteSaveTask);

        void ExecuteSaveTask()
        {
            var names = new List<string>();

            foreach (var item in Tasks)
            {
                if (string.IsNullOrEmpty(item.Name))
                {
                    ShowError("保存失败，无流程名！");
                    return;
                }

                if (names.Contains(item.Name))
                {

                    ShowError("保存失败，重复的流程名！");
                    return;

                }
                else
                {
                    names.Add(item.Name);
                }

            }


            foreach (var item in Tasks)
            {
                if (!_DatasContext.Tasks.Contains(item))
                {

                    _DatasContext.Tasks.Add(item);
                }

            }
            _DatasContext.SaveChanges();
            ShowError("保存成功！");
        }

        private DelegateCommand _AddTask;
        public DelegateCommand AddTask =>
            _AddTask ??= new DelegateCommand(ExecuteAddTask);

        void ExecuteAddTask()
        {
            Tasks.Add(new());
        }
    }
}
