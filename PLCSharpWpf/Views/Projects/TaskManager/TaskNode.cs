﻿using PLCSharp.Views.Projects.TaskManager;
using System;
using System.ComponentModel.DataAnnotations;

namespace PLCSharp.Views.Projects.TaskManager
{
    public class TaskNode
    {
        [Key]
        public string Name { get; set; }
        public string TaskName { get; set; }
        public int NodeID { get; set; }

        public int NextNodeID { get; set; }

        public TaskType Type { get; set; }

        public DateTime StartTime { get; set; }

        public int OverTimeSecond { get; set; }
    }
}