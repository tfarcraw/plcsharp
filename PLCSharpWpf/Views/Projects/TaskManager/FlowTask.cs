﻿using Microsoft.EntityFrameworkCore;
using PLCSharp.Views.GlobalVariables;
using PLCSharp.Views.Projects.TaskManager;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace PLCSharp.Views.Projects
{
    public class FlowTask
    {
        [Key]
        public string Name { get; set; }

 
        [NotMapped]
        public BindingList<TaskNode> TaskNodes { get; set; }
 

    }
}
 
