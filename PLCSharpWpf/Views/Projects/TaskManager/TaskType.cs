﻿namespace PLCSharp.Views.Projects.TaskManager
{
    public enum TaskType
    {
        Jump,
        Script,
        Vision,
        Comm,
    }
}