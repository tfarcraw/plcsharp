﻿using CommunityToolkit.Mvvm.ComponentModel;
using CommunityToolkit.Mvvm.Input;
using Microsoft.Win32;
using ModuleCore.Mvvm;
using ModuleCore.Services;
using OpenCvSharp;
using Prism.Commands;
using Prism.Events;
using Prism.Ioc;
using Prism.Mvvm;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Media.Imaging;

namespace PLCSharp.Views.Hardware.Vision
{
    public partial class CameraModel : BindableBase
    {
        public CameraModel(IEventAggregator ea, IContainerExtension container)
        {

            HikrobotCameras = new HikCameras();
            _ea = ea;
            HikrobotCameras.ErrorMessage += ShowError;
            HikrobotCameras.ListChanged += UpdateCameraList;
            HikrobotCameras.InitCameras();

        }

        private void UpdateCameraList(string cameraInfo)
        {
            var info = cameraInfo.Split(";");
            CameraList.Add(info[0], info[1]); // 厂家 ，IP
        }

        private void ShowError(string obj)
        {
            _ea.GetEvent<MessageEvent>().Publish(new()
            {
                Target = "errLog",
                Content = obj
            });
        }
        public ICameras HikrobotCameras { get; set; }
        private readonly IEventAggregator _ea;
        //   [RelayCommand]

        private ObservableDictionary<string, string> _CameraList = [];

        public ObservableDictionary<string, string> CameraList
        {
            get { return _CameraList; }
            set => SetProperty(ref _CameraList, value);
        }


        //System.Windows.Application.Current.Dispatcher.Invoke(new Action(() =>
        //{
        //    ImgDst = WriteableBitmapConverter.ToWriteableBitmap(Dst);
        //}));


    }
}