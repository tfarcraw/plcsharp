﻿using System.ComponentModel.DataAnnotations;


namespace PLCSharp.Views.Hardware.Vision
{
    public class CameraConfig
    {
        [Key]
        public string Name { get; set; }


        public string IP { get; set; }

        /// <summary>
        /// 注释
        /// </summary>
        public string Comments { get; set; }


    }



}