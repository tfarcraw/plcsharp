﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PLCSharp.Views.Hardware.ControlCard
{
   public class ControlCardConfig
    {
        [Key]
        public string Name { get; set; }


        public string IP { get; set; }

        /// <summary>
        /// 注释
        /// </summary>
        public string Comments { get; set; }
    }
}
