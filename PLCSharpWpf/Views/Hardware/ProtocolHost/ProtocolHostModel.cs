﻿using DryIoc.ImTools;
using ModuleCore.Common;
using PLCSharp.Models;
using PLCSharp.Views.Hardware.ControlCard;
using PLCSharp.Views.Hardware.ProtocolHost;
using PLCSharp.Views.Hardware.ProtocolHost.Socket;
using PLCSharp.Views.Hardware.Vision;
using Prism.Commands;
using Prism.Dialogs;
using Prism.Events;
using Prism.Ioc;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Net;
using System.Windows;

namespace PLCSharp.Views.Hardware.ProtocolHost
{
    [Model]
    public class ProtocolHostModel : ModelBase
    {
        private readonly IDialogService _dialogService;
        public ProtocolHostModel(IContainerExtension container, IEventAggregator ea, IDialogService dialogService) : base(container, ea)
        {

         
            _dialogService = dialogService;
            foreach (var item in _DatasContext.ProtocolHosts)
            {
                ProtocolHostConfigs.Add(item);
   
            }
        }

        protected override void OnExit(object sender, EventArgs e)
        {
        }

        #region ProtocolHost

        private BindingList<ProtocolHostConfig> _ProtocolHostConfigs = [];

        public BindingList<ProtocolHostConfig> ProtocolHostConfigs
        {
            get { return _ProtocolHostConfigs; }
            set { SetProperty(ref _ProtocolHostConfigs, value); }
        }

        public List<SocketClient> SocketClients { get; set; } = [];

        private ProtocolHostConfig _SelectedProtocolHost;

        public ProtocolHostConfig SelectedProtocolHost
        {
            get { return _SelectedProtocolHost; }
            set { SetProperty(ref _SelectedProtocolHost, value); }
        }

        private DelegateCommand<string> _ProtocolHostManage;

        public DelegateCommand<string> ProtocolHostManage =>
            _ProtocolHostManage ??= new DelegateCommand<string>(ExecuteProtocolHostManage);

        private void ExecuteProtocolHostManage(string cmd)
        {
            switch (cmd)
            {
                case "New":
                    ProtocolHostConfigs.Add(new());
                    break;

                case "Save":
                    SaveProtocolHosts();
                    break;
                case "Config":
                    switch (SelectedProtocolHost.Type)
                    {
                        case ProtocolType.SocketClient:

                            var client = SocketClients.Where(c => c.Name == SelectedProtocolHost.Name).FirstOrDefault();



                            if (client == null)
                            {
                                SocketClients.Add(new SocketClient()
                                {
                                    Name = SelectedProtocolHost.Name,
                                    ServerIP = SelectedProtocolHost.Host.ServerIP,
                                    ServerPort = SelectedProtocolHost.Host.ServerPort,
                                });
                            }
                            else {

                                client.ServerIP = SelectedProtocolHost.Host.ServerIP;
                                client.ServerPort = SelectedProtocolHost.Host.ServerPort;
                            }

            

                            _dialogService.ShowDialog("SocketClientConfig", new DialogParameters($"Name={SelectedProtocolHost.Name}"));

                            break;
                        case ProtocolType.SocketSever:
                            break;
                        case ProtocolType.ModbusTcpClient:
                            break;
                        case ProtocolType.ModbusTcpServer:
                            break;
                        default:
                            break;
                    }
                    break;

                case "Remove":
                    if (SelectedProtocolHost != null)
                    {
                        if (_DatasContext.ProtocolHosts.Any(h => h.ID == SelectedProtocolHost.ID))

                            _DatasContext.ProtocolHosts.Remove(SelectedProtocolHost);

                        _DatasContext.SaveChanges();
                        var name = SelectedProtocolHost.Name;
                        ProtocolHostConfigs.Remove(SelectedProtocolHost);
                        ShowError($"已删除：{name}");
                    }
                    break;

            }
        }



        private void SaveProtocolHosts()
        {
            var names = new List<string>();

            foreach (var item in ProtocolHostConfigs)
            {
                if (string.IsNullOrEmpty(item.Name))
                {
                    ShowError($"保存失败，名称{item.Name}不合适！");
                    return;
                }

                if (names.Contains(item.Name))
                {
                    ShowError($"保存失败，重复的名称{item.Name}！");
                    return;
                }
                else
                {
                    names.Add(item.Name);
                }
            }


            foreach (var item in ProtocolHostConfigs)
            { 
                if (!_DatasContext.ProtocolHosts.Any(h => h.ID == item.ID))
                {
                    _DatasContext.ProtocolHosts.Add(item);
 
                }
                
            }
            SelectedProtocolHost.IsModified = false;
            _DatasContext.SaveChanges();
        }

     


        #endregion ProtocolHost

     
    }
}