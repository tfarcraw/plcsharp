﻿using ModuleCore.Mvvm;
using Prism.Ioc;
using Prism.Navigation.Regions;


namespace PLCSharp.Views.Hardware.ProtocolHost.SerialPortConfig
{
    public class SerialPortViewModel : RegionViewModelBase
    {
        public SerialPortViewModel(IContainerExtension container, IRegionManager regionManager) : base(regionManager)
        {
            Model = container.Resolve<SerialPortModel>();
        }

        public SerialPortModel Model { get; set; }
    }
}