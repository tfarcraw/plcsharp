﻿using Newtonsoft.Json;
using Prism.Mvvm;
using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace PLCSharp.Views.Hardware.ProtocolHost
{
    public class ProtocolHostConfig : BindableBase
    {

 
        public Guid ID { get; set; } = Guid.NewGuid();

        public string Name
        {
            get { return name; }
            set
            {
                IsModified = true;
                name = value;
            }
        }
        private string name;

        private bool _IsModified;
        [NotMapped]
        public bool IsModified
        {
            get { return _IsModified; }
            set { SetProperty(ref _IsModified, value); }
        }

        /// <summary>
        /// 注释
        /// </summary>
        public string Comments { get; set; }
        public ProtocolType Type { get; set; }
 

        [NotMapped]

        public ProtocolHost Host { get; set; } = new ProtocolHost();


 
    }

}