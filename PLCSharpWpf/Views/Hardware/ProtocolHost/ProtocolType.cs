﻿namespace PLCSharp.Views.Hardware.ProtocolHost
{
    public enum ProtocolType
    {
        SocketClient,
        SocketSever,
        ModbusTcpClient,
        ModbusTcpServer,
    }
}