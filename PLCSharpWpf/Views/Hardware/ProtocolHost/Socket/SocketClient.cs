﻿using System;
using System.Collections.Concurrent;
using System.ComponentModel;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace PLCSharp.Views.Hardware.ProtocolHost.Socket
{
    public class SocketClient
    {
        public string Name { get; set; }
        private TcpClient _client;
        /// <summary>
        /// 是否是连接的
        /// </summary>
        public bool Connected => _client?.Connected ?? false;
        public SocketClient()
        {
            AppDomain.CurrentDomain.ProcessExit += OnExit;
            bkgWorker = new BackgroundWorker { WorkerSupportsCancellation = true };
            bkgWorker.DoWork += BackgroundWork;

            if (!bkgWorker.IsBusy)
                bkgWorker.RunWorkerAsync();
        }


        public string ServerIP { get; set; }

        public int ServerPort { get; set; }

        public ReceiveDataType Type { get; set; }

        public event Action<string> ReceiveEvent;

        public event Action<byte[]> ReceiveDateEvent;

        public ConcurrentQueue<string> ReceiveQueue { get; set; } = [];
        public ConcurrentQueue<string> LogQueue { get; set; } = [];

        private StateEnum State;


        /// <summary>
        /// 连接
        /// </summary>
        private bool Connect()
        {

            if (ServerIP == null) return false;
            if (IPAddress.TryParse(ServerIP, out var ip) == false) return false;
            bool success = false;
            _client?.Close();
            _client = new TcpClient();
    

            try
            {
                _client.Connect(ServerIP, ServerPort);
                success = true;
                Log($"{DateTime.Now} ;{ServerIP}:{ServerPort}连接成功");

            }
            catch (Exception ex)
            {
                Log($"{ServerIP}:{ServerPort}无法连接...,{ex.Message}");
            }
            return success;
        }

        /// <summary>
        /// 发送消息
        /// </summary>
        public async Task<bool> SendMsgAsync(string msg)
        {
            if (string.IsNullOrEmpty(msg)) return false;
            byte[] byteData = Encoding.UTF8.GetBytes(msg);
            if (_client != null && _client.Connected)
            {
                try
                {
                    await _client.GetStream().WriteAsync(byteData.AsMemory(0, byteData.Length));
                    Log($"{DateTime.Now:HH:mm:ss:fff}向{ServerIP}发送信息 => {msg} ");
                    return true;
                }
                catch (Exception ex)
                {
                    Log("SendMsg Exception: " + ex.Message);
                    return false;
                }

            }
            else
            {
                return false;
            }
        }
        public async Task<bool> SendDataAsync(byte[] bytes)
        {
            if (bytes == null || bytes.Length == 0) return false;

            if (_client != null && _client.Connected)
            {
                try
                {
                    await _client.GetStream().WriteAsync(bytes.AsMemory(0, bytes.Length));
                    Log($"{DateTime.Now:HH:mm:ss:fff}向{ServerIP}发送信息 => {string.Join('_', bytes)} ");
                    return true;
                }
                catch (Exception ex)
                {
                    Log("SendMsg Exception: " + ex.Message);
                    return false;
                }

            }
            else
            {
                return false;
            }
        }

        public bool LogSwitch { get; set; }
        public string ReceiveString { get; private set; }

        public byte[] ReceiveBytes { get; private set; } = [];
        private void Log(string log)
        {
            if (LogSwitch)
            { LogQueue.Enqueue(log); }
        }

        private readonly byte[] _buff = new byte[1024];

        /// <summary>
        /// 接收消息
        /// </summary>
        private bool ReceiveMsg()
        {
            if (_client != null && _client.Connected)
            {
                //下面这个指令会造成m_buffRece数据丢失！
                //Array.Clear(m_buffRece, 0, m_buffRece.Length);


                try
                {
                    var receiveDataLen = _client.GetStream().Read(_buff, 0, _buff.Length);

                    // Thread.Sleep(100); // 这个没影响
                    // await Task.Delay(100); //会丢数据,因为 await 之后，接管的线程不一定是原来的线程

                    //服务器异常断开会一直收到 0 长度的信息
                    if (receiveDataLen == 0)
                    {
                        return false;
                    }
                    if (Type == ReceiveDataType.String)
                    {
                        var msg = Encoding.UTF8.GetString(_buff, 0, receiveDataLen);
                        ReceiveString = msg;
                        ReceiveEvent?.Invoke(msg);
                        ReceiveQueue.Enqueue(msg);
                        Log($"{DateTime.Now:HH:mm:ss:fff} 接收{ServerIP}信息 <= " + msg);
                    }
                    else
                    {
                        ReceiveBytes = _buff[0..(receiveDataLen - 1)];
                        ReceiveDateEvent?.Invoke(ReceiveBytes);
                        ReceiveQueue.Enqueue(string.Join('_', ReceiveBytes));
                        Log($"{DateTime.Now:HH:mm:ss:fff} 接收{ServerIP}信息 <= " + string.Join('_', ReceiveBytes));

                    }

                    //以上两个指令必须一起执行，中间不能有异步操作，否则_buff里的数据会丢失！！！

                    return true;
                }
                catch (Exception ex)
                {
                    Log("ReceMsg Exception: " + ex.Message);

                    return false;
                }
            }
            else
            {
                return false;
            }
        }
        private readonly BackgroundWorker bkgWorker;
        private void OnExit(object sender, EventArgs e)
        {
            Close();
            bkgWorker.CancelAsync();
            bkgWorker.Dispose();
        }
        private void BackgroundWork(object sender, DoWorkEventArgs e)
        {
            var worker = (BackgroundWorker)sender;

            while (!worker.CancellationPending)
            {
                try
                {
                    switch (State)
                    {
                        case StateEnum.Init:
                            {
                                if (Connect())
                                    State = StateEnum.Receive;
                                else
                                    State = StateEnum.Sleep;
                                break;
                            }
                        case StateEnum.Receive:
                            {
                                if (ReceiveMsg())
                                {
                                    break;
                                }
                                else
                                {

                                    State = StateEnum.Sleep;
                                    break;
                                }
                            }

                        case StateEnum.Sleep:
                            {
                                Close();
                                Thread.Sleep(1000);
                                State = StateEnum.Init;
                                break;
                            }
                    }

                }
                catch (Exception ex)
                {
                    Log($"Error: {ex.Message}");
                    Thread.Sleep(1);
                }


            }
        }
        /// <summary>
        ///关闭连接
        /// </summary>
        private void Close()
        {
            _client?.Close();
        }



    }
    public enum StateEnum
    {
        Init,
        Receive,
        Sleep
    }

    public enum ReceiveDataType
    {

        String,
        Bytes,
    }
}
