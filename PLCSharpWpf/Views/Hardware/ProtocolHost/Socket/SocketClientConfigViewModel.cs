﻿using DryIoc;
using ModuleCore.Mvvm;
using PLCSharp.Common;
using Prism.Commands;
using Prism.Dialogs;
using Prism.Ioc;
using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Net;
using System.Net.NetworkInformation;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;

namespace PLCSharp.Views.Hardware.ProtocolHost.Socket
{
    public class SocketClientConfigViewModel : ViewModelBase, IDialogAware
    {
        public SocketClientConfigViewModel(IContainerExtension container)
        {
            _ProtocolHostModel = container.Resolve<ProtocolHostModel>();
            bkgWorker = new BackgroundWorker { WorkerSupportsCancellation = true };
            bkgWorker.DoWork += BackgroundWork;

            if (!bkgWorker.IsBusy)
                bkgWorker.RunWorkerAsync();
        }

        private ProtocolHostModel _ProtocolHostModel;
        private string _Title = "Socket Client";

        public string Title
        {
            get { return _Title; }
            set
            {
                SetProperty(ref _Title, value);
            }
        }

        public DialogCloseListener RequestClose { get; }

        public bool CanCloseDialog()
        {
            return true;
        }

        public void OnDialogClosed()
        {
            client.LogSwitch = false;
            bkgWorker.CancelAsync();
            bkgWorker.Dispose();
        }

        public void OnDialogOpened(IDialogParameters parameters)
        {
            var name = parameters.GetValue<string>("Name");

             client = _ProtocolHostModel.SocketClients.Where(c => c.Name == name).FirstOrDefault();

            IP = client.ServerIP;
            Port = client.ServerPort;
            LogSwitch = true;
            client.LogSwitch = true;
        }

        private readonly BackgroundWorker bkgWorker;

        private void BackgroundWork(object sender, DoWorkEventArgs e)
        {
            var worker = (BackgroundWorker)sender;

            while (!worker.CancellationPending)
            {
                Thread.Sleep(10);
                if (client != null)
                {
                    if (Connected != client.Connected)
                        Connected = client.Connected;

                    client.LogSwitch = LogSwitch;
                    if (LogSwitch)
                    {
                        if (client.LogQueue.Count > 0)
                        {
                            if (client.LogQueue.TryDequeue(out string log))

                                System.Windows.Application.Current.Dispatcher.Invoke(new Action(() =>
                                {
                                    Logs.Add(new() { Time = DateTime.Now, Message = log });
                                }));
                        }
                    }
                }
                //如果连接断开，Ping
                if (Connected == false)
                {
                    PingExist = PingIP(IP).Result;
                }
                else
                {
                    PingExist = true;

                }
            }
        }
        private readonly Ping Pinger = new();
        private async Task<bool> PingIP(string ip)
        {
            var isip = IPAddress.TryParse(ip, out var ipAddress);
            if (isip == false)
            {
                return false;
            }
            bool isMatch = Regex.IsMatch(ip, @"^([1-9]\d?|1\d{2}|2[01]\d|22[0-3])(\.([1-9]?\d|1\d{2}|2[0-4]\d|25[0-5])){3}$");
            if (isMatch == false)
            {
                return false;
            }
            try
            {
                var result = await Pinger.SendPingAsync(ipAddress);
                return result.Status == IPStatus.Success;
            }
            catch (Exception ex)
            {

                return false;
            }
        }

        private bool _LogSwitch;

        public bool LogSwitch
        {
            get { return _LogSwitch; }
            set { SetProperty(ref _LogSwitch, value); }
        }

        public SocketClient client;

        //日志
        private BindingList<Log> _Logs = [];

        public BindingList<Log> Logs
        {
            get { return _Logs; }
            set { SetProperty(ref _Logs, value); }
        }

        // IP
        private string _IP = "127.0.0.1";

        [Required(ErrorMessage = "IP不能为空！")]
        [RegularExpression(@"^([1-9]\d?|1\d{2}|2[01]\d|22[0-3])(\.([1-9]?\d|1\d{2}|2[0-4]\d|25[0-5])){3}$", ErrorMessage = "IP地址格式不正确")]
        public string IP
        {
            get { return _IP; }
            set { SetProperty(ref _IP, value); }
        }

        // PORT
        private int _Port = 7950;

        [Required(ErrorMessage = "端口不能为空！")]
        [Range(0, 65535, ErrorMessage = "端口应在0-65535之间.")]
        public int Port
        {
            get { return _Port; }
            set { SetProperty(ref _Port, value); }
        }

        private bool _PingExist;

        public bool PingExist
        {
            get { return _PingExist; }
            set { SetProperty(ref _PingExist, value); }
        }

        private bool _Connected;

        public bool Connected
        {
            get { return _Connected; }
            set { SetProperty(ref _Connected, value); }
        }

        private string _SendString = "abc;time;1";

        public string SendString
        {
            get { return _SendString; }
            set
            {
                SetProperty(ref _SendString, value);
            }
        }

        #region Command

        private DelegateCommand _Send;

        public DelegateCommand Send =>
            _Send ??= new DelegateCommand(ExecuteSend);

        private async void ExecuteSend()
        {
            if (client != null)
                await client.SendMsgAsync(_SendString);
        }



        #endregion Command
    }
}