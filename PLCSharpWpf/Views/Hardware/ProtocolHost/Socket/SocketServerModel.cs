﻿using ModuleCore.Mvvm;
using ModuleCore.Services;
using Prism.Commands;
using Prism.Events;
using System;
using System.ComponentModel.DataAnnotations;
using System.Data;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace PLCSharp.Views.Hardware.ProtocolHost.Socket
{
    public class SocketServerModel : ValidateService
    {
        public SocketServerModel(IEventAggregator eventAggregator)
        {
            server = new SocketServer()
            {
            };
            server.LogString += LogMsg;
            server.ReceiveResult += ReceiveResult;
            _eventAggregator = eventAggregator;
            _eventAggregator.GetEvent<MessageEvent>().Subscribe(MessageReceived, filter => filter.Target.Contains("socketserver"));
            GetClientCount();
        }

        private void ReceiveResult(string arg1, System.Net.Sockets.Socket socket)
        {
        }

        private void MessageReceived(Message message)
        {
        }

        private async void GetClientCount()
        {
            while (true)
            {
                await Task.Delay(1000);
                ClientCount = server.clientScoketList.Count;
            }
        }

        private void MessageReceived(string obj)
        {
            var msg = obj.Replace("server;", "");
            server.SendMsg(msg);
        }

        private readonly IEventAggregator _eventAggregator;

        private void ReceiveResult(string msg)
        {
            //Debug.WriteLine(Thread.CurrentThread.ManagedThreadId.ToString());
            msg = msg.Replace("\r", "");
            msg = msg.Replace("\n", "");

            _eventAggregator.GetEvent<MessageEvent>().Publish(new()
            {
                Target = "socketserver",
                Content = msg
            });
        }

        private SocketServer server;

        private int _ClientCount;

        public int ClientCount
        {
            get { return _ClientCount; }
            set { SetProperty(ref _ClientCount, value); }
        }

        //日志
        private string _ServerLog = string.Empty;

        public string ServerLog
        {
            get { return _ServerLog; }
            set { SetProperty(ref _ServerLog, value); }
        }

        private string _ClientLog = string.Empty;

        public string ClientLog
        {
            get { return _ClientLog; }
            set { SetProperty(ref _ClientLog, value); }
        }

        private readonly StringBuilder logmsg = new();
        private readonly object o = new();

        private void LogMsg(string msg)
        {
            lock (o)
            {
                logmsg.Insert(0, "\r\n");
                logmsg.Insert(0, msg);

                string log = "";
                try
                {
                    if (logmsg.Length > 1000)
                    {
                        string logmegui = logmsg.ToString();
                        var idx = logmegui.LastIndexOf('\n');
                        logmsg.Remove(idx, logmsg.Length - idx);
                    }
                    log = logmsg.ToString();
                }
                catch
                {
                    Console.WriteLine(log);
                }
                Application.Current?.Dispatcher.Invoke(() => ServerLog = log);
            }
        }

        // IP
        private string _ip = "127.0.0.1";

        [Required(ErrorMessage = "IP不能为空！")]
        [RegularExpression(@"^([1-9]\d?|1\d{2}|2[01]\d|22[0-3])(\.([1-9]?\d|1\d{2}|2[0-4]\d|25[0-5])){3}$", ErrorMessage = "IP地址格式不正确")]
        public string ServerIP
        {
            get { return _ip; }
            set { SetProperty(ref _ip, value); }
        }

        // PORT
        private int _port = 7950;

        [Required(ErrorMessage = "端口不能为空！")]
        [Range(0, 65535, ErrorMessage = "端口应在0-65535之间.")]
        public int ServerPort
        {
            get { return _port; }
            set { SetProperty(ref _port, value); }
        }

  

        private string _sendText;

        public string SendText
        {
            get { return _sendText; }
            set { SetProperty(ref _sendText, value); }
        }

        #region Command

        private DelegateCommand _Send;

        public DelegateCommand Send =>
            _Send ??= new DelegateCommand(ExecuteSend);

        private void ExecuteSend()
        {
            server.SendMsg(_sendText);
        }

        private DelegateCommand _Load;

        public DelegateCommand Load =>
            _Load ??= new DelegateCommand(ExecuteLoad);

        private void ExecuteLoad()
        {
        }

        private DelegateCommand _Save;

        public DelegateCommand Save =>
            _Save ??= new DelegateCommand(ExecuteSave);

        private void ExecuteSave()
        {
        }

        #endregion Command
    }
}