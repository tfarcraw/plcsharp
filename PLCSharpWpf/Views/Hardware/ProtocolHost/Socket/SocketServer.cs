﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace PLCSharp.Views.Hardware.ProtocolHost.Socket
{
    public class SocketServer
    {
        public readonly List<System.Net.Sockets.Socket> clientScoketList = [];//存储 连接 服务器 的 客户端
        public readonly List<System.Net.Sockets.Socket> badclientScoketList = [];//存储 失效 的 客户端

        public event Action<string> LogString;         // 连接日志

        public event Action<string, System.Net.Sockets.Socket> ReceiveResult;

        public event Action<System.Net.Sockets.Socket, int> Clients;

        private Thread thread;

        private System.Net.Sockets.Socket socketServer;

        public bool Open(string ip, int port)
        {
            try
            {

                IPEndPoint iPEndPoint = new(IPAddress.Parse(ip), port);

                if (socketServer != null && socketServer.LocalEndPoint.ToString() == $"{ip}:{port}")
                {
                    return true;
                }
                if (socketServer != null)
                {

                    socketServer.Close();
                    foreach (var proxSocket in clientScoketList)
                    {
                        if (!badclientScoketList.Contains(proxSocket))
                        {
                            badclientScoketList.Add(proxSocket);
                        }
                    }
                    ClearBad();
                    thread?.Interrupt();
                };
                //1、创建Socket对象 参数：寻址方式，当前为Ipv4  指定套接字类型   指定传输协议Tcp；
                socketServer = new(AddressFamily.InterNetwork, SocketType.Stream, System.Net.Sockets.ProtocolType.Tcp);
                //2、绑定端口、IP


                socketServer.Bind(iPEndPoint);

                //3、开启侦听   10为队列最多接收的数量
                socketServer.Listen();

                //4、开始接受客户端的连接  ，连接会阻塞主线程，故使用线程池。

                thread = new Thread(new ThreadStart(AcceptClientConnect))
                {
                    IsBackground = true
                };
                thread.Start();

                return true;
            }
            catch (Exception ex)
            {
                //    throw;
                SendLog($"{DateTime.Now:HH:mm:ss:fff} {ex.Message}");
                return false;
            }
        }

        /// <summary>
        /// 线程池线程执行的接受客户端连接方法
        /// </summary>
        /// <param name="obj">传入的Socket</param>
        private void AcceptClientConnect()
        {
            //转换Socket

            SendLog($"{DateTime.Now:HH:mm:ss:fff} 服务端{socketServer.LocalEndPoint}已准备好！");

            //不断接受客户端的连接
            while (true)
            {
                try
                {
                    Thread.Sleep(100);
                    //5、创建一个负责通信的Socket

                    System.Net.Sockets.Socket proxSocket = socketServer.Accept();
                    if (proxSocket == null)
                        continue;

                    var client = proxSocket.RemoteEndPoint.ToString();
                    SendLog($"{DateTime.Now:HH:mm:ss:fff} 客户端：{client} 连接上了！");
                    _ = Task.Run(() => Clients?.Invoke(proxSocket, 1));

                    //将连接的Socket存入集合

                    clientScoketList.Add(proxSocket);
                    //6、不断接收客户端发送来的消息
                    Task.Run(() => ReceiveClientMsg(proxSocket));
                }
                catch (Exception ex)
                {

                    SendLog($"SendMsg:{ex.Message}");

                }
            }
        }



        /// <summary>
        /// 不断接收客户端信息子线程方法
        /// </summary>
        /// <param name="obj">参数Socke对象</param>
        private void ReceiveClientMsg(System.Net.Sockets.Socket proxSocket)
        {
            var client = proxSocket.RemoteEndPoint.ToString();
            //创建缓存内存，存储接收的信息, 不能放到while中，这块内存可以循环利用
            byte[] data = new byte[1020 * 1024];
            while (true)
            {
                int len;
                try
                {
                    //接收消息,返回字节长度
                    len = proxSocket.Receive(data, 0, data.Length, SocketFlags.None);
                }
                catch (Exception ex)
                {
                    //7、关闭Socket

                    ClientExit($"{DateTime.Now:HH:mm:ss:fff}  客户端：{client}异常常退出：{ex.Message}", proxSocket);
                    return;//让方法结束，终结当前客户端数据的异步线程，方法退出，即线程结束
                }

                if (len <= 0)//判断接收的字节数   小于0表示正常退出
                {
                    ClientExit($"{DateTime.Now:HH:mm:ss:fff}  客户端：{client}正常退出", proxSocket);
                    return;//让方法结束，终结当前客户端数据的异步线程，方法退出，即线程结束
                }
                //将消息显示到Log
                string msgStr = Encoding.UTF8.GetString(data, 0, len);
                if (string.IsNullOrEmpty(msgStr))
                {
                    return;
                }
                //拼接字符串

                SendLog($"{DateTime.Now:HH:mm:ss:fff} 接收到[{client}]的消息<=：{msgStr.Replace("\r\n", "\\r\\n")}");
                _ = Task.Run(() => ReceiveResult?.Invoke($"{msgStr}", proxSocket));
            }
        }

        /// <summary>
        /// 消息广播
        /// </summary>
        public bool SendMsg(string str)
        {
            if (string.IsNullOrEmpty(str))
            {
                return false;
            }

            byte[] data = Encoding.UTF8.GetBytes(str);

            if (clientScoketList.Count > 0)
            {
                SendLog($"{DateTime.Now:HH:mm:ss:fff} SendMsg: 向 {clientScoketList.Count} 个客户端发送=>{str}");
                foreach (var proxSocket in clientScoketList)
                {
                    if (proxSocket.Connected)//判断客户端是否还在连接
                    {
                        _ = proxSocket.Send(data, 0, data.Length, SocketFlags.None); //指定套接字的发送行为
                    }
                    else
                    {
                        badclientScoketList.Add(proxSocket);
                        SendLog("SendMsg:客户端未连接");
                        ClearBad();
                        return false;
                    }
                }
            }
            else
            {
                SendLog("SendMsg:没有客户机接入");
                return false;
            }

            return true;
        }

        private void ClearBad()
        {
            foreach (var proxSocket in badclientScoketList)
            {
                if (clientScoketList.Contains(proxSocket))
                {
                    if (proxSocket.Connected)//如果是连接状态
                    {
                        proxSocket.Shutdown(SocketShutdown.Both);//关闭连接
                        proxSocket.Close(100);//100秒超时间
                    }
                    clientScoketList.Remove(proxSocket);
                    _ = Task.Run(() => Clients?.Invoke(proxSocket, 0));
                }
            }
            badclientScoketList.Clear();
        }

        /// <summary>
        /// 向指定客户端发送消息
        /// </summary>
        /// <param name="str"></param>
        /// <param name="proxSocket"></param>
        /// <returns></returns>
        public bool SendMsg(string str, System.Net.Sockets.Socket proxSocket)
        {
            if (string.IsNullOrEmpty(str))
            {
                return false;
            }

            if (proxSocket.Connected)//判断客户端是否还在连接
            {
                byte[] data = Encoding.UTF8.GetBytes(str);

                _ = proxSocket.Send(data, 0, data.Length, SocketFlags.None); //指定套接字的发送行为
                string clinet = proxSocket.RemoteEndPoint.ToString();
                SendLog($"{DateTime.Now:HH:mm:ss:fff} 向连接[{clinet}]发信息=>:{str}");
                return true;
            }
            else
            {
                SendLog($"{proxSocket}未连接");
                badclientScoketList.Add(proxSocket);
                ClearBad();
                return false;
            }
        }

        /// <summary>
        /// 客户端退出调用
        /// </summary>
        /// <param name="msg"></param>
        private void ClientExit(string msg, System.Net.Sockets.Socket proxSocket)
        {
            _ = Task.Run(() => LogString?.Invoke(msg));

            badclientScoketList.Add(proxSocket);//移除集合中的连接Socket
            ClearBad();
        }

        private void SendLog(string msg)
        {
 
            _ = Task.Run(() => LogString?.Invoke(msg));
        }



    }
}
