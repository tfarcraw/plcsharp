﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PLCSharp.Views.Hardware.ProtocolHost
{
    /// <summary>
    /// 网络变量
    /// </summary>
    /// <typeparam name="T">变量类型</typeparam>
    public class NetVariable
    {
        public bool IsSucceed { get; set; }

        public int ErrCode { get; set; }

        public string ErrInfo { get; set; }

        public string Address { get; set; }


        public Type Type { get; set; }
        public dynamic Value { get; set; }
        /// <summary>
        /// 请求报文
        /// </summary>
        public string Requst { get; set; }

        /// <summary>
        /// 响应报文
        /// </summary>
        public string Response { get; set; }



    }
}
