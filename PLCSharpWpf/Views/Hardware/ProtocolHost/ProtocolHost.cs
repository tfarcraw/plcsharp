﻿
using Microsoft.EntityFrameworkCore;
using ModuleCore.Services;
using Prism.Mvvm;
using System;
using System.Collections.Generic;
using System.DirectoryServices.ActiveDirectory;
using System.Linq;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;

namespace PLCSharp.Views.Hardware.ProtocolHost
{
 
    public   class  ProtocolHost :BindableBase
    {

        private bool _Connected;
        /// <summary>
        /// 连接成功
        /// </summary>
        public bool Connected
        {
            get { return _Connected; }
            set { SetProperty(ref _Connected, value); }
        }

        private bool _Successful;
        /// <summary>
        /// 读写成功
        /// </summary>
        public bool Successful
        {
            get { return _Successful; }
            set { SetProperty(ref _Successful, value); }
        }

        private bool _PingExist;
        public bool PingExist
        {
            get { return _PingExist; }
            set { SetProperty(ref _PingExist, value); }
        }

        public string ServerIP { get; set; }

        public int ServerPort { get; set; }


    }
}
