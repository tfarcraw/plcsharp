﻿using ModuleCore.Services;
using System.ComponentModel.DataAnnotations;

namespace PLCSharp.Views.Hardware
{
    public class DeviceConfig : ValidateService
    {
        private string _IP;

        [Required(ErrorMessage = "不能为空！")]
        [RegularExpression(@"^([1-9]\d?|1\d{2}|2[01]\d|22[0-3])(\.([1-9]?\d|1\d{2}|2[0-4]\d|25[0-5])){3}$", ErrorMessage = "IP地址格式不正确")]
        public string IP
        {
            get { return _IP; }
            set
            {
                SetProperty(ref _IP, value);
                if (IsValidated)
                    ConfigChange = true;
            }
        }

        private uint _Port;

        public uint Port
        {
            get { return _Port; }
            set { SetProperty(ref _Port, value); }
        }

        private bool _IsOnline;

        public bool IsOnline
        {
            get { return _IsOnline; }
            set { SetProperty(ref _IsOnline, value); }
        }

        private bool _IsConnected;

        public bool IsConnected
        {
            get { return _IsConnected; }
            set { SetProperty(ref _IsConnected, value); }
        }

        private bool _IsRWSuccess;

        public bool IsRWSuccess
        {
            get { return _IsRWSuccess; }
            set { SetProperty(ref _IsRWSuccess, value); }
        }

        public bool ConfigChange { get; set; }
    }
}