﻿using ModuleCore.Common;
using ModuleCore.Views.Authority;
using System.Windows.Controls;

namespace PLCSharp.Views.Hardware
{
    [NavigationPage(ViewName = "Hardware",
       IconKind = "\ue687",
       DisplayName = "硬件配置", UserLevel = Authority.Administrator)]
    public partial class Hardware : UserControl
    {
        public Hardware()
        {
            InitializeComponent();
        }
    }
}