﻿using DryIoc.ImTools;
using ModuleCore.Common;
using PLCSharp.Models;
using PLCSharp.Views.Hardware.ControlCard;
using PLCSharp.Views.Hardware.ProtocolHost;
using PLCSharp.Views.Hardware.ProtocolHost.Socket;
using PLCSharp.Views.Hardware.Vision;
using Prism.Commands;
using Prism.Dialogs;
using Prism.Events;
using Prism.Ioc;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Net;
using System.Windows;

namespace PLCSharp.Views.Hardware
{
    [Model]
    public class HardwaresModel : ModelBase
    {
        private readonly IDialogService _dialogService;
        public HardwaresModel(IContainerExtension container, IEventAggregator ea, IDialogService dialogService) : base(container, ea)
        {

            _dialogService = dialogService;
       
        }

        protected override void OnExit(object sender, EventArgs e)
        {
        }

  

        #region Camera

        private BindingList<CameraConfig> _Cameras = [];

        public BindingList<CameraConfig> Cameras
        {
            get { return _Cameras; }
            set { SetProperty(ref _Cameras, value); }
        }

        private CameraConfig _SelectedCamera;

        public CameraConfig SelectedCamera
        {
            get { return _SelectedCamera; }
            set { SetProperty(ref _SelectedCamera, value); }
        }


        private DelegateCommand _CamreraManage;
        public DelegateCommand CamreraManage =>
            _CamreraManage ??= new DelegateCommand(ExecuteCamreraManage);

        void ExecuteCamreraManage()
        {

        }
 
        #endregion Camera
        #region ControlCard

        private BindingList<ControlCardConfig> _ControlCards = [];

        public BindingList<ControlCardConfig> ControlCards
        {
            get { return _ControlCards; }
            set { SetProperty(ref _ControlCards, value); }
        }

        private ControlCardConfig _SelectedCard;

        public ControlCardConfig SelectedCard
        {
            get { return _SelectedCard; }
            set { SetProperty(ref _SelectedCard, value); }
        }


        private DelegateCommand<string> _ControlCardManage;
        public DelegateCommand<string> ControlCardManage =>
            _ControlCardManage ??= new DelegateCommand<string>(ExecuteControlCardManage);

        void ExecuteControlCardManage(string cmd)
        {

        }
        #endregion ControlCard
    }
}