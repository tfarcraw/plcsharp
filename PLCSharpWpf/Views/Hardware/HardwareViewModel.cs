﻿
using PLCSharp.Views.Hardware.ProtocolHost;
using Prism.Ioc;
using Prism.Mvvm;

namespace PLCSharp.Views.Hardware
{
    public class HardwareViewModel : BindableBase
    {

        public HardwareViewModel(IContainerExtension container)
        {
      
            protocolHostModel = container.Resolve<ProtocolHostModel>();

        }

        public HardwaresModel Model { get; set; }

        public ProtocolHostModel protocolHostModel { get; set; }

    }
}
