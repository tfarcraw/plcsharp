﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PLCSharp.wwwroot
{
    public class HelloController
    {
        [HttpGet("/")]
        public string Get() => "Hello";
    }
}
