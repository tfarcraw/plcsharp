﻿using ModuleCore.Mvvm;
using Prism.Events;
using Prism.Ioc;
using Prism.Mvvm;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PLCSharp.Models
{
  public  class ModelBase: BindableBase
    {
        IEventAggregator _EventAggregator;
        public DatasContext _DatasContext { get; set; }
        public ModelBase(IContainerExtension container, IEventAggregator ea)
        {
            _DatasContext = container.Resolve<DatasContext>();
            _EventAggregator = ea;
            AppDomain.CurrentDomain.ProcessExit += OnExit;
 
        }

        protected virtual void OnExit(object sender, EventArgs e)
        {
 
        }

        public void ShowError(string msg)
        {
            _EventAggregator.GetEvent<MessageEvent>().Publish(new()
            {
                Target = "errLog",
                Content = msg
            });
        }
 
    }
}
