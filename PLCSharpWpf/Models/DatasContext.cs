﻿using DryIoc.ImTools;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Storage;
using Microsoft.Extensions.Hosting;
using Newtonsoft.Json;
using PLCSharp.Views.GlobalVariables;
using PLCSharp.Views.Hardware;
using PLCSharp.Views.Hardware.ControlCard;
using PLCSharp.Views.Hardware.ProtocolHost;
using PLCSharp.Views.Hardware.Vision;
using PLCSharp.Views.Projects;
using PLCSharp.Views.Projects.TaskManager;
using System;
using System.Diagnostics;
using System.Linq;
using System.Xml;

namespace PLCSharp.Models
{
    public class DatasContext : DbContext
    {
        public DatasContext() {

           // this.Database.EnsureCreated();
            this. EnsureCreatingMissingTables();

        }
        public DbSet<Variable> Variables { get; set; }
        public DbSet<FlowTask> Tasks { get; set; }
        public DbSet<TaskNode> TaskNodes { get; set; }
        public DbSet<ProtocolHostConfig> ProtocolHosts { get; set; }
        public DbSet<CameraConfig> Cameras { get; set; }
        public DbSet<ControlCardConfig> ControlCards { get; set; }



        protected override void OnConfiguring(DbContextOptionsBuilder options)
            => options.UseSqlite("Data Source=Datas.db");

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<ProtocolHostConfig>()
                .Property(e => e.Host)
                .HasConversion(
                    v => JsonConvert.SerializeObject(v), // 序列化
                    v => JsonConvert.DeserializeObject<ProtocolHost>(v) // 反序列化
                );
        }
    }
 

    internal static class DbContextExtensions
    {
        internal static void EnsureCreatingMissingTables<TDbContext>(this TDbContext dbContext) where TDbContext : DbContext
        {
            var type = typeof(TDbContext);
            var dbSetType = typeof(DbSet<>);

            var dbPropertyNames = type.GetProperties().Where(p => p.PropertyType.Name == dbSetType.Name)
                .Select(p => p.Name).ToArray();

            foreach (var entityName in dbPropertyNames)
            {
                CheckTableExistsAndCreateIfMissing(dbContext, entityName);
            }
        }

        private static void CheckTableExistsAndCreateIfMissing(DbContext dbContext, string entityName)
        {
            var defaultSchema = dbContext.Model.GetDefaultSchema();
            var tableName = string.IsNullOrWhiteSpace(defaultSchema) ? $"{entityName}" : $"{defaultSchema}.{entityName}";

            try
            {
                var sqlstr = $"SELECT * FROM {tableName} LIMIT 1";
                _ = dbContext.Database.ExecuteSqlRaw(sqlstr); //Throws on missing table
            }
            catch (Exception)
            {
                var scriptStart = $"CREATE TABLE \"{tableName}\"";
             
                var script = dbContext.Database.GenerateCreateScript();

                var tableScript = script.Split(scriptStart).Last().Split(";");

                var first = $"{scriptStart} {tableScript.First()}";
                if(first is not null)
                    dbContext.Database.ExecuteSqlRaw(first);
    
            }
        }
    }
}
