﻿namespace PLCSharp.PLC.FB.Sensor
{
    public readonly struct ST_SensorErr
    {
        public readonly bool[] abErr = new bool[8];
        public readonly string[] astrErr = new string[8];

        public ST_SensorErr()
        {
        }
    }
}