﻿namespace PLCSharp.PLC.FB.Cyl
{
    public readonly struct ST_CylErr
    {
        public readonly bool[] abErr = new bool[8];
        public readonly string[] astrErr = new string[8];

        public ST_CylErr()
        {
        }
    }
}