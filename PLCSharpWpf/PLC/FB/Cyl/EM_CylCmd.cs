﻿namespace PLCSharp.PLC.FB.Cyl
{
    public enum EM_CylCmd
    {
        NOP,
        HP,
        WP,
        Stop
    }
}