﻿using CommunityToolkit.Mvvm.ComponentModel;
using PLCSharp.PLC.Packml;

namespace PLCSharp.HMI
{
    public partial class ObservableState : ObservableObject

    {
        private ST_PackmlState state = new();

        public ST_PackmlState State
        {
            get
            {
                return state;
            }
            set
            {
                state = value;
                Clearing = state.bClearing;
                Stopped = state.bStopped;
                Starting = state.bStarting;
                Idle = state.bIdle;
                Suspended = state.bSuspended;
                Execute = state.bExecute;
                Aborted = state.bAborted;
                Stopping = state.bStopping;
                Aborting = state.bAborting;
                Holding = state.bHolding;
                Held = state.bHeld;
                UnHolding = state.bUnHolding;
                Suspending = state.bSuspending;
                UnSuspending = state.bUnSuspending;
                Resetting = state.bResetting;
                Completing = state.bCompleting;
                Complete = state.bCompleting;
            }
        }

        [ObservableProperty]
        private bool clearing;    //    清除中

        [ObservableProperty]
        private bool stopped;  //    停止

        [ObservableProperty]
        private bool starting;  //    正在启动

        [ObservableProperty]
        private bool idle;  //    空闲

        [ObservableProperty]
        private bool suspended;  //    外部暂停

        [ObservableProperty]
        private bool execute;  //    执行

        [ObservableProperty]
        private bool stopping;  //    停止中

        [ObservableProperty]
        private bool aborting;  //    中止中

        [ObservableProperty]
        private bool aborted;  //    中止

        [ObservableProperty]
        private bool holding;  //    内部暂停进入中

        [ObservableProperty]
        private bool held;  //    内部暂停

        [ObservableProperty]
        private bool unHolding;  //    内部暂停解除中

        [ObservableProperty]
        private bool suspending;  //    外部暂停进入中

        [ObservableProperty]
        private bool unSuspending;  //    外部暂停解除中

        [ObservableProperty]
        private bool resetting;  //    复位中

        [ObservableProperty]
        private bool completing;  //    完成中

        [ObservableProperty]
        private bool complete;  //    完成
    }
}