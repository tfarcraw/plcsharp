﻿using CommunityToolkit.Mvvm.ComponentModel;
using CommunityToolkit.Mvvm.Input;

using Prism.Ioc;

using System.ComponentModel;
using System.Threading.Tasks;

namespace PLCSharp.HMI
{
    public partial class HMI : ObservableObject
    {
        public HMI(IContainerExtension container)
        {
            PLC = container.Resolve<PLC.PLC>();

            for (int i = 0; i < 10; i++)
            {
                DO.Add(false);
            }
            UpdateHMI();
        }

        public PLC.PLC PLC { get; set; }

        [ObservableProperty]
        private BindingList<bool> _DO = new();

        [ObservableProperty]
        private int _BlinkCount;
        [ObservableProperty]
        private string _BarCode;
        [ObservableProperty]
        private ObservableState _State = new();
        [RelayCommand]
        private void NewBarCodeEnter() { }
        private async void UpdateHMI()
        {
            while (true)
            {
                await Task.Delay(100);
                for (int i = 0; i < PLC.DO.Count; i++)
                {
                    DO[i] = PLC.DO[i];
                }
                BlinkCount = PLC.intTrigCount;

                State.State = PLC.state;
            }
        }

        [ObservableProperty]
        [NotifyCanExecuteChangedFor(nameof(StartCommand))]
        private bool enableStart = true;

        [RelayCommand(CanExecute = nameof(EnableStart))]
        private async Task StartAsync()
        {
            RunState = "运行中";
            await Task.Delay(1000);
            RunState = "启动";
        }

        [ObservableProperty]
        private string runState = "启动";


    }
}