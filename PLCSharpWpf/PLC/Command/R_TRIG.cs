﻿namespace PLCSharp.Command
{
    public class R_TRIG
    {
        public bool IN { get; set; }

        private bool _Q;

        public bool Q
        {
            get
            {
                return _Q;
            }
        }

        public void FB(bool _IN)
        {
            IN = _IN;

            if (temp == false && IN == true)
            {
                temp = true;
                _Q = true;
            }
            else
            {
                _Q = false;
            }

            if (IN == false)
            {
                temp = false;
            }
        }

        private bool temp;
    }
}