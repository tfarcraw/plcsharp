﻿using System;

namespace PLCSharp.Command
{
    public class TON
    {
        public bool IN { get; set; }

        private bool _Q;

        public bool Q
        {
            get
            {
                return _Q;
            }
        }

        public double PT { get; set; }

        public double ET
        {
            get
            {
                if (IN == false)
                {
                    return PT;
                }
                if (DateTime.Now > timeEnd)
                {
                    return 0;
                }
                else
                {
                    return (timeEnd - DateTime.Now).TotalSeconds;
                }
            }
        }

        public void FB(bool _IN, double _PT)
        {
            IN = _IN;

            if (IN == false)
            {
                _Init = false;
                _Q = false;
                return;
            }

            if (IN && !_Init)  //开始计时
            {
                _Init = true;
                PT = _PT;
                var pt = TimeSpan.FromSeconds(_PT);
                timeEnd = DateTime.Now.Add(pt);
            }

            if (DateTime.Now > timeEnd) _Q = true;
        }

        private bool _Init;

        private DateTime timeEnd;
    }
}