﻿namespace PLCSharp.Command
{
    public class F_TRIG
    {
        public bool IN { get; set; }

        private bool _Q;

        public bool Q
        {
            get
            {
                return _Q;
            }
        }

        public void FB(bool _IN)
        {
            IN = _IN;

            if (temp == true && IN == false)
            {
                temp = false;
                _Q = true;
            }
            else
            {
                _Q = false;
            }

            if (IN == true)
            {
                temp = true;
            }
        }

        private bool temp;
    }
}