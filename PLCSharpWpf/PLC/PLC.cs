﻿using System.Collections.Generic;
using System.Threading;

namespace PLCSharp.PLC
{
    public partial class PLC
    {
        public PLC()
        {
            for (int i = 0; i < 10; i++)
            {
                DO.Add(false);
            }
            Start();
        }

        public void Start()
        {
            Thread run = new(() =>
            {
                while (true)
                {
                    Thread.Sleep(1); //扫描周期1ms
                    MainPou();
                }
            })
            {
                IsBackground = true
            };
            run.Start();
        }

        public List<bool> DO = [];
    }
}