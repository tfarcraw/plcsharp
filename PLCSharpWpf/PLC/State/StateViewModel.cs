﻿using CommunityToolkit.Mvvm.ComponentModel;
using CommunityToolkit.Mvvm.Input;

using Prism.Ioc;

using System.Threading.Tasks;

namespace PLCSharp.Views.State
{
    public partial class StateViewModel : ObservableObject
    {
        public StateViewModel(IContainerExtension container)
        {
            PLC = container.Resolve<PLC.PLC>();
            HMI = container.Resolve<HMI.HMI>();
        }

        public PLC.PLC PLC { get; set; }
        public HMI.HMI HMI { get; set; }

        [ObservableProperty]
        [NotifyCanExecuteChangedFor(nameof(StartCommand))]
        private bool enableStart = true;

        [RelayCommand(CanExecute = nameof(EnableStart))]
        private async Task StartAsync()
        {
            RunState = "运行中";
            await Task.Delay(1000);
            RunState = "启动";
        }

        [ObservableProperty]
        private string runState = "启动";
    }
}