﻿namespace PLCSharp.PLC.Packml
{
    public partial class ST_PackmlState
    {
        public bool bUndefined;  //    未定义

        public bool bClearing;  //    清除中

        public bool bStopped;  //    停止

        public bool bStarting;  //    正在启动

        public bool bIdle;  //    空闲

        public bool bSuspended;  //    外部暂停

        public bool bExecute;  //    执行

        public bool bStopping;  //    停止中

        public bool bAborting;  //    中止中

        public bool bAborted;  //    中止

        public bool bHolding;  //    内部暂停进入中

        public bool bHeld;  //    内部暂停

        public bool bUnHolding;  //    内部暂停解除中

        public bool bSuspending;  //    外部暂停进入中

        public bool bUnSuspending;  //    外部暂停解除中

        public bool bResetting;  //    复位中

        public bool bCompleting;  //    完成中

        public bool bComplete;  //    完成
    }
}