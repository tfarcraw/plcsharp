﻿namespace PLCSharp.PLC.Packml
{
    public struct ST_PackmlSC
    {
        public bool bReset;
        public bool bResettingSC;
        public bool bStart;
        public bool bStartingSC;
        public bool bStop;
        public bool bStoppingSC;
        public bool bHold;
        public bool bHoldingSC;
        public bool bUnHold;
        public bool bUnHoldingSC;
        public bool bSuspend;
        public bool bSuspendingSC;
        public bool bUnSuspend;
        public bool bUnSuspendingSC;
        public bool bAbort;
        public bool bAbortingSC;
        public bool bClear;
        public bool bClearingSC;
        public bool bExecuteSC;
        public bool bCompletingSC;
    }
}