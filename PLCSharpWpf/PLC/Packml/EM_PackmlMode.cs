﻿namespace PLCSharp.PLC.Packml;

public enum EM_PackmlMode
{
    Production = 1,   //生产模式 用于由机器重复的生产。
    Maintenance = 2,  //维修模式 用于非安全模式下手动操作设备
    Manual = 3        //手动模式 用于手动操作设备
}