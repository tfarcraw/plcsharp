﻿namespace PLCSharp.PLC.Packml
{
    public static class FC_PackmlState
    {
        public static void FC(EM_PackmlState io_emPackmlState, ST_PackmlSC i_stPackmlSC, ST_PackmlState o_stPackmlState)
        {
            switch (io_emPackmlState)
            {
                case EM_PackmlState.Clearing:       // 1
                    if (i_stPackmlSC.bClearingSC) io_emPackmlState = EM_PackmlState.Stopped;
                    if (i_stPackmlSC.bAbort) io_emPackmlState = EM_PackmlState.Aborting;
                    break;

                case EM_PackmlState.Stopped:        // 2
                    if (i_stPackmlSC.bReset) io_emPackmlState = EM_PackmlState.Resetting;
                    if (i_stPackmlSC.bAbort) io_emPackmlState = EM_PackmlState.Aborting;
                    break;

                case EM_PackmlState.Starting:       // 3
                    if (i_stPackmlSC.bStartingSC) io_emPackmlState = EM_PackmlState.Execute;
                    if (i_stPackmlSC.bStop) io_emPackmlState = EM_PackmlState.Stopping;
                    if (i_stPackmlSC.bAbort) io_emPackmlState = EM_PackmlState.Aborting;
                    break;

                case EM_PackmlState.Idle:           // 4
                    if (i_stPackmlSC.bStart) io_emPackmlState = EM_PackmlState.Starting;
                    if (i_stPackmlSC.bStop) io_emPackmlState = EM_PackmlState.Stopping;
                    if (i_stPackmlSC.bAbort) io_emPackmlState = EM_PackmlState.Aborting;
                    break;

                case EM_PackmlState.Suspended:      // 5
                    if (i_stPackmlSC.bUnSuspend) io_emPackmlState = EM_PackmlState.UnSuspending;
                    if (i_stPackmlSC.bStop) io_emPackmlState = EM_PackmlState.Stopping;
                    if (i_stPackmlSC.bAbort) io_emPackmlState = EM_PackmlState.Aborting;
                    break;

                case EM_PackmlState.Execute:        // 6
                    if (i_stPackmlSC.bExecuteSC) io_emPackmlState = EM_PackmlState.Completing;
                    if (i_stPackmlSC.bHold) io_emPackmlState = EM_PackmlState.Holding;
                    if (i_stPackmlSC.bSuspend) io_emPackmlState = EM_PackmlState.Suspending;
                    if (i_stPackmlSC.bStop) io_emPackmlState = EM_PackmlState.Stopping;
                    if (i_stPackmlSC.bAbort) io_emPackmlState = EM_PackmlState.Aborting;
                    break;

                case EM_PackmlState.Stopping:       // 7
                    if (i_stPackmlSC.bStoppingSC) io_emPackmlState = EM_PackmlState.Stopped;
                    if (i_stPackmlSC.bAbort) io_emPackmlState = EM_PackmlState.Aborting;
                    break;

                case EM_PackmlState.Aborting:       // 8
                    if (i_stPackmlSC.bAbortingSC) io_emPackmlState = EM_PackmlState.Aborted;
                    break;

                case EM_PackmlState.Aborted:        // 9
                    if (i_stPackmlSC.bClear) io_emPackmlState = EM_PackmlState.Clearing;
                    break;

                case EM_PackmlState.Holding:        // 10
                    if (i_stPackmlSC.bHoldingSC) io_emPackmlState = EM_PackmlState.Held;
                    if (i_stPackmlSC.bStop) io_emPackmlState = EM_PackmlState.Stopping;
                    if (i_stPackmlSC.bAbort) io_emPackmlState = EM_PackmlState.Aborting;
                    break;

                case EM_PackmlState.Held:             // 11
                    if (i_stPackmlSC.bUnHold) io_emPackmlState = EM_PackmlState.UnHolding;
                    if (i_stPackmlSC.bStop) io_emPackmlState = EM_PackmlState.Stopping;
                    if (i_stPackmlSC.bAbort) io_emPackmlState = EM_PackmlState.Aborting;
                    break;

                case EM_PackmlState.UnHolding:          // 12
                    if (i_stPackmlSC.bUnHoldingSC) io_emPackmlState = EM_PackmlState.Execute;
                    if (i_stPackmlSC.bStop) io_emPackmlState = EM_PackmlState.Stopping;
                    if (i_stPackmlSC.bAbort) io_emPackmlState = EM_PackmlState.Aborting;
                    break;

                case EM_PackmlState.Suspending:      // 13
                    if (i_stPackmlSC.bSuspendingSC) io_emPackmlState = EM_PackmlState.Suspended;
                    if (i_stPackmlSC.bStop) io_emPackmlState = EM_PackmlState.Stopping;
                    if (i_stPackmlSC.bAbort) io_emPackmlState = EM_PackmlState.Aborting;
                    break;

                case EM_PackmlState.UnSuspending:        // 14
                    if (i_stPackmlSC.bUnSuspendingSC) io_emPackmlState = EM_PackmlState.Execute;
                    if (i_stPackmlSC.bStop) io_emPackmlState = EM_PackmlState.Stopping;
                    if (i_stPackmlSC.bAbort) io_emPackmlState = EM_PackmlState.Aborting;
                    break;

                case EM_PackmlState.Resetting:      // 15
                    if (i_stPackmlSC.bResettingSC) io_emPackmlState = EM_PackmlState.Idle;
                    if (i_stPackmlSC.bStop) io_emPackmlState = EM_PackmlState.Stopping;
                    if (i_stPackmlSC.bAbort) io_emPackmlState = EM_PackmlState.Aborting;
                    break;

                case EM_PackmlState.Completing:      // 16
                    if (i_stPackmlSC.bCompletingSC) io_emPackmlState = EM_PackmlState.Complete;
                    if (i_stPackmlSC.bStop) io_emPackmlState = EM_PackmlState.Stopping;
                    if (i_stPackmlSC.bAbort) io_emPackmlState = EM_PackmlState.Aborting;
                    break;

                case EM_PackmlState.Complete:           // 17
                    if (i_stPackmlSC.bReset) io_emPackmlState = EM_PackmlState.Resetting;
                    if (i_stPackmlSC.bStop) io_emPackmlState = EM_PackmlState.Stopping;
                    if (i_stPackmlSC.bAbort) io_emPackmlState = EM_PackmlState.Aborting;
                    break;

                default:
                    io_emPackmlState = EM_PackmlState.Aborted;
                    break;
            }

            if (io_emPackmlState == EM_PackmlState.Clearing) o_stPackmlState.bClearing = true; else o_stPackmlState.bClearing = false;
            if (io_emPackmlState == EM_PackmlState.Stopped) o_stPackmlState.bStopped = true; else o_stPackmlState.bStopped = false;
            if (io_emPackmlState == EM_PackmlState.Starting) o_stPackmlState.bStarting = true; else o_stPackmlState.bStarting = false;
            if (io_emPackmlState == EM_PackmlState.Idle) o_stPackmlState.bIdle = true; else o_stPackmlState.bIdle = false;

            if (io_emPackmlState == EM_PackmlState.Suspended) o_stPackmlState.bSuspended = true; else o_stPackmlState.bSuspended = false;
            if (io_emPackmlState == EM_PackmlState.Execute) o_stPackmlState.bExecute = true; else o_stPackmlState.bExecute = false;
            if (io_emPackmlState == EM_PackmlState.Stopping) o_stPackmlState.bStopping = true; else o_stPackmlState.bStopping = false;
            if (io_emPackmlState == EM_PackmlState.Aborting) o_stPackmlState.bAborting = true; else o_stPackmlState.bAborting = false;

            if (io_emPackmlState == EM_PackmlState.Aborted) o_stPackmlState.bAborted = true; else o_stPackmlState.bAborted = false;
            if (io_emPackmlState == EM_PackmlState.Holding) o_stPackmlState.bHolding = true; else o_stPackmlState.bHolding = false;
            if (io_emPackmlState == EM_PackmlState.Held) o_stPackmlState.bHeld = true; else o_stPackmlState.bHeld = false;
            if (io_emPackmlState == EM_PackmlState.UnHolding) o_stPackmlState.bUnHolding = true; else o_stPackmlState.bUnHolding = false;

            if (io_emPackmlState == EM_PackmlState.Suspending) o_stPackmlState.bSuspending = true; else o_stPackmlState.bSuspending = false;
            if (io_emPackmlState == EM_PackmlState.UnSuspending) o_stPackmlState.bUnSuspending = true; else o_stPackmlState.bUnSuspending = false;
            if (io_emPackmlState == EM_PackmlState.Resetting) o_stPackmlState.bResetting = true; else o_stPackmlState.bResetting = false;
            if (io_emPackmlState == EM_PackmlState.Completing) o_stPackmlState.bCompleting = true; else o_stPackmlState.bCompleting = false;

            if (io_emPackmlState == EM_PackmlState.Complete) o_stPackmlState.bComplete = true; else o_stPackmlState.bComplete = false;
        }
    }
}