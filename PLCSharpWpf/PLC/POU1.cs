﻿using PLCSharp.Command;
using PLCSharp.PLC.Packml;

namespace PLCSharp.PLC
{
    public partial class PLC
    {
        private readonly TON ton1 = new();
        private readonly TON ton2 = new();
        private readonly R_TRIG rTRIG1 = new();
        private readonly F_TRIG fTRIG1 = new();
        public int intTrigCount;
        private int intDO;

        public ST_PackmlState state = new();

        private void Pou1()
        {
            ton1.FB(!ton2.Q, 0.1);
            ton2.FB(ton1.Q, 0.1);
            rTRIG1.FB(ton1.Q);
            fTRIG1.FB(ton1.Q);

            if (rTRIG1.Q || fTRIG1.Q)
                intTrigCount++;

            if (ton2.Q)
            {
                if (intDO < 10) intDO++;
                if (intDO >= 10) intDO = 0;

                state.bAborted = !state.bAborted;
                for (int i = 0; i < 10; i++)
                {
                    if (intDO == i)
                        DO[i] = true;
                    else
                        DO[i] = false;
                }
            }
        }
    }
}