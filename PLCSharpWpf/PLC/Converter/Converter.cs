﻿using System;
using System.Collections.Generic;

namespace PLCSharp.Converter
{
    public static class Converter
    {
        #region int

        public static bool[] ToBitArray(this int intValue)
        {
            bool[] bitArray = new bool[32];
            for (var i = 0; i <= 31; i++)
            {
                var val = 1 << i;
                bitArray[i] = (intValue & val) == val;
            }
            return bitArray;
        }

        public static bool GetBit(this int intValue, int index)
        {
            if (index > 31 || index < 0) throw new Exception("索引越界");

            var val = 1 << index;
            return (intValue & val) == val;
        }

        public static void SetBit(ref this int intValue, int index)
        {
            if (index > 31 || index < 0) throw new Exception("索引越界");

            var val = 1 << index;

            intValue |= val;
        }

        public static void RstBit(ref this int intValue, int index)
        {
            if (index > 31 || index < 0) throw new Exception("索引越界");

            var val = 1 << index;

            intValue &= ~val;
        }

        #endregion int

        #region byte

        public static bool[] ToBitArray(this byte byteValue)
        {
            bool[] bitArray = new bool[8];
            for (var i = 0; i < 8; i++)
            {
                var val = 1 << i;
                bitArray[i] = (byteValue & val) == val;
            }
            return bitArray;
        }

        public static void Bit(ref this byte byteValue, int index, bool tf)
        {
            if (index >= 8 || index < 0) throw new Exception("索引越界");

            var val = 1 << index;
            if (tf) byteValue |= (byte)val; else byteValue &= (byte)~val;
        }

        public static bool GetBit(this byte byteValue, int index)
        {
            if (index >= 8 || index < 0) throw new Exception("索引越界");

            var val = 1 << index;
            return (byteValue & val) == val;
        }

        public static void SetBit(ref this byte byteValue, int index)
        {
            if (index >= 8 || index < 0) throw new Exception("索引越界");

            var val = 1 << index;

            byteValue |= (byte)val;
        }

        public static void RstBit(ref this byte byteValue, int index)
        {
            if (index >= 8 || index < 0) throw new Exception("索引越界");

            var val = 1 << index;

            byteValue &= (byte)~val;
        }

        private static IEnumerable<bool> GetBits(byte b)
        {
            for (int i = 0; i < 8; i++)
            {
                yield return b % 2 != 0;
                b = (byte)(b >> 1);
            }
        }

        #endregion byte
    }
}