﻿using System;

namespace PLCSharp.Converter
{
    public static class Int16Extension
    {
        public static void setBit(ref this ushort value, int index)
        {
            if (value < 0 || value > 15)
            {
                throw new ArgumentException("index 必须在 0 - 15 之间");
            }
            var valueTemp = value;

            for (ushort i = 0; i < 16; i++)
            {
                X[i] = valueTemp % 2 == 1;
                valueTemp >>= 1;
            }
            X[index] = true;
            valueTemp = 0;
            for (ushort i = 0; i < 16; i++)
            {
                if (X[i]) { valueTemp += (ushort)Math.Pow(2, i); }
            }
            value = valueTemp;
        }

        public static void resetBit(ref this ushort value, int index)
        {
            if (value < 0 || value > 15)
            {
                throw new ArgumentException("index 必须在 0 - 15 之间");
            }
            var valueTemp = value;

            for (ushort i = 0; i < 16; i++)
            {
                X[i] = valueTemp % 2 == 1;
                valueTemp >>= 1;
            }
            X[index] = false;
            valueTemp = 0;
            for (ushort i = 0; i < 16; i++)
            {
                if (X[i]) { valueTemp += (ushort)Math.Pow(2, i); }
            }
            value = valueTemp;
        }

        public static bool[] X { get; set; } = new bool[16];

        public static bool getBit(this ushort value, int index)
        {
            if (value < 0 || value > 15)
            {
                throw new ArgumentException("index 必须在 0 - 15 之间");
            }
            var valueTemp = value;

            for (ushort i = 0; i < 16; i++)
            {
                X[i] = valueTemp % 2 == 1;
                valueTemp >>= 1;
            }
            return X[index];
        }

        public static void setBit(ref this short value, int index)
        {
            if (value < 0 || value > 15)
            {
                throw new ArgumentException("index 必须在 0 - 15 之间");
            }
            var valueTemp = value;

            for (ushort i = 0; i < 16; i++)
            {
                X[i] = valueTemp % 2 == 1;
                valueTemp >>= 1;
            }
            X[index] = true;
            valueTemp = 0;
            for (ushort i = 0; i < 16; i++)
            {
                if (X[i]) { valueTemp += (short)Math.Pow(2, i); }
            }
            value = valueTemp;
        }

        public static void resetBit(ref this short value, int index)
        {
            if (value < 0 || value > 15)
            {
                throw new ArgumentException("index 必须在 0 - 15 之间");
            }
            var valueTemp = value;

            for (ushort i = 0; i < 16; i++)
            {
                X[i] = valueTemp % 2 == 1;
                valueTemp >>= 1;
            }
            X[index] = false;
            valueTemp = 0;
            for (ushort i = 0; i < 16; i++)
            {
                if (X[i]) { valueTemp += (short)Math.Pow(2, i); }
            }
            value = valueTemp;
        }

        public static bool getBit(this short value, int index)
        {
            if (value < 0 || value > 15)
            {
                throw new ArgumentException("index 必须在 0 - 15 之间");
            }
            var valueTemp = value;

            for (ushort i = 0; i < 16; i++)
            {
                X[i] = valueTemp % 2 == 1;
                valueTemp >>= 1;
            }
            return X[index];
        }
    }
}