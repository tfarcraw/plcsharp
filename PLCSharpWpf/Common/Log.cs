﻿using Prism.Mvvm;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PLCSharp.Common
{
    public class Log : BindableBase
    {

        private string _Message;
        public string Message
        {
            get { return _Message; }
            set { SetProperty(ref _Message, value); }
        }

        private DateTime _Time;
        public DateTime Time
        {
            get { return _Time; }
            set { SetProperty(ref _Time, value); }
        }
    }
}
