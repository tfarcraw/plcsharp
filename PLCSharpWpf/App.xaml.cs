﻿using DryIoc;
using Microsoft.AspNetCore.Builder;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using ModuleCore.Common;
using ModuleCore.Mvvm;
using ModuleCore.Views.PasswordChange;
using PLCSharp.Models;
using PLCSharp.Views.GlobalVariables;
using PLCSharp.Views.Hardware;
using PLCSharp.Views.Hardware.ProtocolHost;
using PLCSharp.Views.Hardware.ProtocolHost.Socket;
using PLCSharp.Views.Projects;
using Prism.Ioc;
using Prism.Modularity;
using Prism.Mvvm;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Windows;

namespace PLCSharp
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App
    {
        protected override Window CreateShell()
        {
            var builder = WebApplication.CreateBuilder();
            builder.Services.AddControllers();
            var app = builder.Build();

            // Configure the HTTP request pipeline.
            if (!app.Environment.IsDevelopment())
            {
                app.UseExceptionHandler("/Error");
            }
            app.UseStaticFiles();
            app.UseRouting();

            app.MapControllers();
            //  app.MapGet("/", () => "Hello World!");
            app.RunAsync();

            //System.Environment.SetEnvironmentVariable("Path", @"D:/OpenCV/Runtimes/opencv452/;D:/OpenCV/Runtimes/paddleocr/;D:/OpenCV/Runtimes/zbarX64Runtime/;D:/OpenCV/Runtimes/BaslerRuntimeX64/;D:/OpenCV/Runtimes/MVSRuntimeX64/;C:\Program Files\Basler\pylon 6\Runtime\x64;C:\Program Files (x86)\Common Files\MVS\Runtime\Win64_x64;");
            //反射需要导航的页面

            var Navigate = Container.Resolve<NavigateModel>();
            var views = from t in Assembly.GetExecutingAssembly().GetTypes()   //
                        where t.GetCustomAttribute<NavigationPageAttribute>() is not null
                        select t;
            List<NavigateItem> nls = new List<NavigateItem>();
            foreach (var view in views)
            {
                var page = view.GetCustomAttribute<NavigationPageAttribute>();
                nls.Add(new NavigateItem()
                {
                    ViewName = page.ViewName,
                    IconKind = page.IconKind,
                    DisplayName = page.DisplayName,
                    UserLevel = page.UserLevel,
                    Index = page.Index
                });
            }

            nls.Sort();
            foreach (var view in nls)
            {
                Navigate.NavigateList.Add(view);
            }
            //设置默认页面
            Navigate.DefaultView = "First";
            //启动Core中的主窗体
            return null;
        }

        //最先执行
        protected override void RegisterTypes(IContainerRegistry containerRegistry)
        {
            //Module中的初始化晚于这里，所以不能在Module中注册全局使用的实例，比如NavigateModel
            //注册导航菜单
            _ = containerRegistry.RegisterSingleton<NavigateModel>();
            var views = from t in Assembly.GetExecutingAssembly().GetTypes()   //
                        where t.GetCustomAttribute<NavigationPageAttribute>() is not null
                        select t;
            foreach (var view in views)
            {
                containerRegistry.RegisterForNavigation(view, view.Name);
            }
            _ = containerRegistry.RegisterSingleton<PLC.PLC>();
            _ = containerRegistry.RegisterSingleton<HMI.HMI>();
            //注入EF
            _ = containerRegistry.RegisterSingleton<DatasContext>();

            //注入Model

            var models = from t in Assembly.GetExecutingAssembly().GetTypes()   //
                        where t.GetCustomAttribute<ModelAttribute>() is not null
                        select t;

            foreach (var model in models) { 
            
            
            
                _ = containerRegistry.RegisterSingleton(model);
            }
           // _ = containerRegistry.RegisterSingleton<ProtocolHostModel>();
   
          

            //注入弹出窗口
            containerRegistry.RegisterDialog<SocketClientConfig, SocketClientConfigViewModel>();

        }

        //第二执行
        protected override void ConfigureModuleCatalog(IModuleCatalog moduleCatalog)
        {
            _ = moduleCatalog.AddModule<ModuleCore.CoreModule>();
        }

        protected override void ConfigureViewModelLocator()
        {
            base.ConfigureViewModelLocator();

            ViewModelLocationProvider.SetDefaultViewTypeToViewModelTypeResolver((viewType) =>
            {
                var viewName = viewType.FullName;
                var viewAssemblyName = viewType.GetTypeInfo().Assembly.FullName;
                var viewModelName = $"{viewName}ViewModel, {viewAssemblyName}";
                return Type.GetType(viewModelName);
            });
        }
    }
}